# -*- coding: utf-8 -*-

{
    "name": "OEC localization",
    "author": "RStudio",
    "website": "https://eis-solution.coding.net/public/odoo/oec/git",
    "sequence": 500,
    "installable": True,
    "application": False,
    "auto_install": True,
    "category": "OEC Suites/Localizations",
    "version": "16.0.0.1",
    "summary": """
        
        """,
    "description": """

        """,
    "license": "Other proprietary",
}
