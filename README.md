![LOGO](doc/img/logo.png)

# `Open Erp China` - 一个基于odoo扩展的中国本土化ERP项目

## `Open Erp China` 介绍

1. 基于Odoo扩展的中国本土化ERP项目，鉴于odoo的版本一年一次更新发布，个人开发者追版本太累。决定跟进odoo的偶数版本。
2. 本项目目标是吸引开发者，开发者可以在此项目开源自己的基础模块。专业收费模块可以通过本人写的widget引流到自己的项目中，从而获得收益。

## 仓库

[![Coding](http://img.shields.io/badge/主仓库-Coding-ee6666.svg?style=flat&colorA=8F8F8F)](https://eis-solution.coding.net/public/odoo/oec/git)
[![Gitee](http://img.shields.io/badge/备用仓库-Gitee-875A7B.svg?style=flat&colorA=8F8F8F)](https://gitee.com/rainbowstudio/oec.git)



## 开发者安装教程

1. 创建 `16.0` 版本的根路径
2. 在根路径 git clone odoo 源代码
   ```
   git clone https://github.com/odoo/odoo.git --depth 1 --branch 16.0 --single-branch src
   ```
3. 在根路径 git clone ChinaERP 源代码
   ```
   git clone git@e.coding.net:eis-solution/odoo/oec.git --depth 1 --branch 16.0 --single-branch oec
   ```
4. 安装 `Odoo` 需要的依赖
   ```
   cd src
   pip install -r requirements.txt -i https://pypi.doubanio.com/simple  
   ```
5. 安装 `ChinaERP` 需要的依赖
   ```
   cd oec
   pip install -r requirements.txt -i https://pypi.doubanio.com/simple  
   ```
6. 从 `src/debian/` 复制文件 `odoo.conf` 到根路径`16.0`下，修改内容为：
   ```
   [options]
   ; admin_passwd 是数据库管理密码
   admin_passwd = admin

   ; 数据库访问的相关参数
   db_host = localhost
   db_port = False
   db_user = odoo
   db_password = odoo
   db_maxconn = 1200

   ; addons_path 是模块路径
   addons_path = src/odoo/addons,src/addons,oec

   ; dbfilter 指定数据库
   dbfilter = oec

   ; 是否启用代理模式
   proxy_mode = True

   ; PG bin安装路径
   pg_path = C:\Program Files\PostgreSQL\14\bin

   data_dir = data_dir
   geoip_database = GeoIP\GeoLite2-City.mmdb
   ```
7. Visual Studio Code 开发 Odoo 配置，[参考链接](https://my.oschina.net/u/138005/blog/1571371)
8. PyCharm 开发 Odoo 配置，待补充

## 开发者模块命名规范
1. 模块名必须以 `oec_` 开头
2. 业务模块名必须以 `oec_` 开头，后面跟业务名。例如如 `oec_sale` 表示销售模块；`oec_account` 表示财务模块；`oec_hr` 表示人力资源模块
3. 业务功能模块名必须以 `oec_` 开头，后面跟业务名，再跟功能名。例如如 `oec_sale_order` 表示销售订单模块；`oec_account_invoice` 表示财务发票模块；`oec_hr_employee` 表示人力资源员工模块

## 开发者如何引流

1. 开发者可以在 视图 `res_config_settings_view_form` 中添加标签 ```<widget name="oec_pro_upgrade_boolean" />``` 来引流到自己的项目中。

2. 具体如下:
   ```
   <div class="col-12 col-lg-6 o_setting_box" id="rainbow_community_theme_install">
      <div class="o_setting_left_pane">
            <field name="module_rainbow_community_theme" widget="oec_pro_upgrade_boolean"/>
      </div>
      <div class="o_setting_right_pane" id="rainbow_community_theme_settings">
         <label for="module_rainbow_community_theme"/>
         <div class="text-muted">
            This module modifies the web addon to provide responsiveness.
         </div>
         <div class="content-group" attrs="{'invisible': [('module_rainbow_community_theme', '=', False)]}" id="rainbow_community_theme_warning">
            <div class="mt4 text-warning">
               <strong>Save</strong> this page and come back here to set up other feature.
            </div>
         </div>
      </div>
      </div>
   </div>
   ```

3. 说明:
- `<widget name="oec_pro_upgrade_boolean" />` 中的 `url` 参数是指向开发者的项目地址，如果不填写则默认为 `https://eis-solution.coding.net/public/odoo/oec/git"`。
- `<widget name="oec_pro_upgrade_boolean" />` 中的 `image` 参数用于加载开发定义图片，如果不填写暂时默认为 `/web/static/img/enterprise_upgrade.jpg`。

## 使用声明

1. 本项目暂供学习交流使用，在 `未声明不得用于商业用途前` 可以商用。

## 打赏/捐赠/悬赏 说明
1. 打赏/捐赠/悬赏 请备注 模块名称，以便于我们将费用及相关悬赏任务安排给对应的代码贡献者。