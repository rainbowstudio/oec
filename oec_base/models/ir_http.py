# -*- coding: utf-8 -*-

import json
from odoo import api, models
from odoo.http import request
from odoo.tools import ustr


class Http(models.AbstractModel):
    _inherit = "ir.http"

    def session_info(self):
        """
        系统信息
        """
        session_info = super(Http, self).session_info()
        del session_info["support_url"]
        
        ICP = self.env["ir.config_parameter"].sudo()
        system_name = ICP.get_param("oec.system_name", default="China ERP")
        display_company_name = bool(ICP.get_param("oec.display_company_name", default=True))

        #用户菜单项 
        current_conpany = self.env.user.company_id
        enable_odoo_account = current_conpany.enable_odoo_account
        enable_lock_screen = current_conpany.enable_lock_screen
        enable_developer_tool = current_conpany.enable_developer_tool
        enable_documentation = current_conpany.enable_documentation
        documentation_url = current_conpany.documentation_url
        enable_support = current_conpany.enable_support
        support_url = current_conpany.support_url


        session_info.update({
            "system_name": system_name,
            "enable_odoo_account": enable_odoo_account,
            "enable_lock_screen": enable_lock_screen,
            "enable_developer_tool": enable_developer_tool,
            "support": {
                "support_url": support_url,
                "hide": enable_support,
            },
            "documentation": {
                "documentation_url": documentation_url,
                "hide": enable_documentation,
            },
        })
        
        if display_company_name:
            session_info.update({"display_company_name": display_company_name})
        return session_info