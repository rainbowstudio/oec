# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    company_id = fields.Many2one(
        "res.company",
        string="Company",
        required=True,
        default=lambda self: self.env.company,
    )

    # 显示在浏览器窗口的标题栏或状态栏上的标题名称
    system_name = fields.Char(
        string="System Name",
        readonly=False,
        config_parameter="oec.system_name",
        default="Open ERP China",
    )
    display_company_name = fields.Boolean(
        string="Display Company Name",
        default=True,
        config_parameter="oec.display_company_name",
    )
    logo = fields.Binary(
        related="company_id.logo", readonly=False
    )
    square_logo = fields.Binary(
        related="company_id.square_logo", readonly=False
    )
    favicon = fields.Binary(
        related="company_id.favicon", readonly=False
    )

    # 主题定制


    disable_theme_customizer = fields.Boolean(
        related="company_id.disable_theme_customizer", readonly=False
    )
    main_menu_mode = fields.Selection(
        related="company_id.main_menu_mode", readonly=False
    )
    main_submenu_position = fields.Selection(
        related="company_id.main_submenu_position", readonly=False
    )

    sidebar_display_number_of_submenus = fields.Boolean(
        related="company_id.sidebar_display_number_of_submenus", readonly=False
    )
    sidebar_fixed = fields.Boolean(related="company_id.sidebar_fixed", readonly=False)
    sidebar_show_minimize_button = fields.Boolean(
        related="company_id.sidebar_show_minimize_button", readonly=False
    )
    sidebar_default_minimized = fields.Boolean(
        related="company_id.sidebar_default_minimized", readonly=False
    )
    sidebar_hover_maximize = fields.Boolean(
        related="company_id.sidebar_hover_maximize", readonly=False
    )

    # 用户菜单
    enable_odoo_account = fields.Boolean(
        related="company_id.enable_odoo_account", readonly=False
    )
    enable_developer_tool = fields.Boolean(
        related="company_id.enable_developer_tool", readonly=False
    )

    enable_lock_screen = fields.Boolean(
        related="company_id.enable_lock_screen", readonly=False
    )
    lock_screen_state_storage_mode = fields.Selection(
        related="company_id.lock_screen_state_storage_mode", readonly=False
    )

    enable_documentation = fields.Boolean(
        related="company_id.enable_documentation", readonly=False
    )
    documentation_url = fields.Char(related="company_id.documentation_url", readonly=False
    )
    enable_support = fields.Boolean(related="company_id.enable_support", readonly=False)
    support_url = fields.Char(related="company_id.support_url", readonly=False)

    # 模块
    module_web_theme_pro = fields.Boolean("Web Theme Professional Edition")
    module_wecom_base= fields.Boolean("Enterprise WeChat")