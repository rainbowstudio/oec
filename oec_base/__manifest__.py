# -*- coding: utf-8 -*-

{
    "name": "OEC Base",
    "author": "RStudio",
    "website": "https://eis-solution.coding.net/public/odoo/oec/git",
    "sequence": 0,
    "installable": True,
    "application": True,
    "auto_install": True,
    "category": "OEC Suites",
    "depends": [
        "web_theme","oec_l10n", "oec_widget",
    ],
    "data": [
        "data/oec_base_setup_data.xml",
        "views/res_groups_views.xml",
        "views/res_users_views.xml",
        "views/res_config_settings_views.xml",
        "views/oec_base_views.xml",
    ],
    "version": "16.0.0.1",
    "summary": """

        """,
    "description": """

        """,
    "assets":{
        "web.assets_backend":{
            # "oec_base/static/src/webclient/*/*"
        },
    },
    "license": "Other proprietary",
}
