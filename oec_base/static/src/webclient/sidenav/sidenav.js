/** @odoo-module **/

import {
    useBus,
    useService
} from "@web/core/utils/hooks";
import {
    SideNav
} from "@web_theme/webclient/sidenav/sidenav";

const {
    Component,
    onMounted,
    onRendered,
    onWillRender,
    onWillDestroy,
    onWillUnmount,
    onPatched,
    onWillUpdateProps,
    useExternalListener,
    useEffect,
    useState,
    useRef
} = owl;

export class OecSideNav extends SideNav {
    setup() {
        super.setup();
        this.router = useService("router");
    }
    onMounted() {
        super.onMounted();
        let menuId = Number(this.router.current.hash.menu_id || 0);
        console.warn("onMounted", menuId, browser.location.hash);
    }
}