# -*- coding: utf-8 -*-
{
    "name": "HRMS Maintenance",
    "author": "RStudio",
    "website": "https://eis-solution.coding.net/public/odoo/oec/git",
    "sequence": 505,
    "installable": True,
    "application": False,
    "auto_install": False,
    "category": "OEC Suites/Human Resources",
    "version": "16.0.0.1",
    "summary": """
        
        """,
    "description": """


        """,
    "depends": ["oec_hr_base", "hr_maintenance",],
    "data": ["views/menu_views.xml", "views/res_config_settings_views.xml"],
    "external_dependencies": {"python": [],},
    "license": "Other proprietary",
}
