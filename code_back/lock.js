odoo.define('web_theme.lock', function (require) {
    'use strict';
    const browser = require('web.core').browser;
    var ajax = require('web.ajax');
    var publicWidget = require('web.public.widget');
    const Dialog = require('web.Dialog');
    const {
        _t,
        qweb
    } = require('web.core');
    const session = require('web.session');


    publicWidget.registry.LockScreen = publicWidget.Widget.extend({
        selector: "#lock-screen form.lock-form",
        template: "Lock.Relogin",
        events: {
            'input #password': '_onInput',
            'click a.o_lock_logout': '_onRelogin',
        },
        start: async function () {
            let self = this;

            var def = this._super.apply(this, arguments);
            this.form = this.el;
            this.error = "";
            this.message = "";
            this.redirect = "";

            this.form.addEventListener('submit', function (event) {
                if (!self.form.checkValidity()) {
                    event.preventDefault()
                    event.stopPropagation()
                } else {
                    event.preventDefault()
                    event.stopPropagation()
                    const alerts = self.form.querySelector('.alert');
                    alerts.classList.add('d-none');
                    self.verifyPassword();
                }
                self.form.classList.add('was-validated')
            }, false)

            return def;
        },
        verifyPassword: function () {
            let self = this;
            const login = this.el.querySelector('input#login').value;
            const password = this.password;
            ajax.jsonRpc('/web/session/unlock', 'call', {
                'login': login,
                'password': password
            }).then(function (result) {
                if (result.hasOwnProperty("error")) {
                    self.error = result.error;
                    self._showError();
                } else if (result.hasOwnProperty("message")) {
                    self.message = result.message;
                    if (result.href) {
                        self.redirect = result.href;
                    } else {
                        self.redirect = "/web";
                    }
                    self._unlock();
                }
            });
        },
        _showError: function () {
            const alert = this.el.querySelector('.alert-danger');
            alert.classList.remove('d-none');
            const span = this.el.querySelector('span.error');
            span.innerHTML = this.error;
        },
        _unlock: function () {
            const alert = this.el.querySelector('.alert-success');
            alert.classList.remove('d-none');
            const span = this.el.querySelector('span.message');
            span.innerHTML = this.message;
            this._countdown();
        },
        _countdown: function () {
            let self = this;
            const countdown = this.el.querySelector('.countdown-line');
            countdown.classList.remove('d-none');
            var timeleft = 3;
            var downloadTimer = setInterval(function () {
                timeleft--;
                self.el.querySelector("span#countdown_number").innerHTML = timeleft;
                if (timeleft <= 0) {
                    clearInterval(downloadTimer);
                    countdown.classList.add('d-none');
                    // browser.location.href = self.redirect;
                    window.location = self.redirect;
                }
            }, 1000);
        },
        _onInput: function (ev) {
            this.password = ev.target.value;
            if (this.password === "") {
                this.el.querySelector('.o_unlock_button').disabled = true;
                this.form.classList.add('was-validated')
            } else {
                this.el.querySelector('.o_unlock_button').disabled = false;
                this.form.classList.remove('was-validated')
            }
        },
        _onRelogin: function (ev) {
            ev.preventDefault();
            var self = this;
            var buttons = [{
                    text: _t("Confirm"),
                    classes: 'btn-danger',
                    close: true,
                    click: this._confirmLogout.bind(this),
                },
                {
                    text: _t("Cancel"),
                    close: true,
                },
            ];

            const lock_user_name = ev.currentTarget.querySelector('b').innerHTML;
            var dialog = new Dialog(this, {
                size: 'medium',
                title: _t("Log out"),
                buttons: buttons,
                $content: qweb.render(self.template, {
                    name: lock_user_name,
                })
            });
            dialog.open();
        },
        _confirmLogout: function (ev) {
            // TODO 需要修改用户的锁屏状态
            window.location.href = "/web/session/logout?redirect=/web/login";
        },
    });
    return publicWidget.registry.LockScreen;
});