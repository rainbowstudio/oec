/** @odoo-module **/

import {
    useService
} from "@web/core/utils/hooks";
import {
    ActionContainer
} from '@web/webclient/actions/action_container';
import {
    CallbackRecorder,
    useSetupAction
} from "@web/webclient/actions/action_hook";

import {
    ScrollTopAction
} from "../scroll_top/scroll_top";
// content
const {
    Component,
    onMounted,
    onRendered,
    onWillUpdateProps,
    onWillDestroy,
    onPatched,
    xml,
    hooks
} = owl;

export class OecActionContainer extends ActionContainer {
    setup() {
        super.setup();
        this.router = useService("router");
        this.currentMenuId = Number(this.router.current.hash.menu_id || 0);
        this.existsScrollEl = false;

        onMounted(() => {
            this.onMounted();
        });
        onRendered(() => {
            this.onRendered();
        });
        onWillUpdateProps(() => {
            this.onWillUpdateProps();
        });
        onWillDestroy(() => {
            this.onWillDestroy();
        });
        onPatched(() => {
            this.onPatched();
        });

    }
    onRendered() {
        // 在渲染组件之前，会用来初始化一些数据等
        // console.log("onRendered", this.currentMenuId);
        // this.el = this.__owl__.bdom.el;
    }
    onMounted() {
        // 在渲染组件之后，
        // console.log("onMounted");
        this.currentMenuId = Number(this.router.current.hash.menu_id || 0);
        // console.log("onMounted", this.currentMenuId);
        this.checkScrollElements();
    }
    async onWillUpdateProps() {
        // 异步，在组件更新之前执行， 方法在组件状态发生改变时调用。
        console.log("willUpdateProps");
    }
    onWillDestroy() {
        // 定义在组件被销毁之前调用。
        console.log("onWillDestroy");
    }
    onPatched() {
        // 在发生改变时执行， 此方法在元素根据新状态重新渲染之前进行调用。
        this.newCurrentMenuId = Number(this.router.current.hash.menu_id || 0);
        // console.log("onPatched", this.currentMenuId, this.newCurrentMenuId);
        this.checkScrollElements();
    }

    //-----------------------------------------------
    // Handlers
    //-----------------------------------------------
    checkScrollElements() {
        // let scrollEl = document.querySelector(".o_content");
        let scrollEl = document.querySelector(".o_renderer");
        //-----------------------------------------------
        // 现发现 以下 元素 需要单独处理
        // 日历: o_content > o_calendar_container > fc-view fc-timeGridWeek-view fc-timeGrid-view > table > fc-body > fc-widget-content >fc-scroller fc-time-grid-container 
        // 仪表盘: o_content o_component_with_search_panel >  o_renderer
        // 常规设置: o_content > class="oe_form_configuration o_base_settings o_form_editable d-block > o_setting_container > settings
        //-----------------------------------------------
        this.existsScrollEl = scrollEl !== null ? true : false;

        if (this.existsScrollEl) {
            // 存在 .o_content 元素

            if (this.currentMenuId !== this.newCurrentMenuId) {
                // 菜单切换
                this.currentMenuId = this.newCurrentMenuId;
                this.checkScrollElements(); //重新检测
            } else {
                // 菜单未切换
                this.showOrHideScrollButton(scrollEl);
            }
        }
    }

    showOrHideScrollButton(scrollEl) {
        let self = this;
        const offset = 300;

        const scroll_top_button = document.querySelector(".o_scroll_top");
        scrollEl.addEventListener("scroll", function (ev) {
            const el = ev.target;
            
            if (el.scrollTop > offset) {
                scroll_top_button.classList.remove("o_hidden");
            } else {
                scroll_top_button.classList.add("o_hidden");
            }
        });

        scroll_top_button.addEventListener("click", function (ev) {
            self.bindScrollActionContainerEvent(ev, scrollEl)
        });
    }

    bindScrollActionContainerEvent(ev, scrollEl) {
        let scroll_top_button = ev.target;
        if (scroll_top_button.tagName == "span") {
            scroll_top_button = scroll_top_button.parentNode;
        } else if (scroll_top_button.tagName == "svg") {
            scroll_top_button = scroll_top_button.parentNode.parentNode;
        } else if (scroll_top_button.tagName == "path") {
            scroll_top_button = scroll_top_button.parentNode.parentNode.parentNode;
        } else if (scroll_top_button.tagName == "rect") {
            scroll_top_button = scroll_top_button.parentNode.parentNode.parentNode;
        }
        const duration = 500; //时间
        ev.preventDefault();
        $(scrollEl).animate({
            scrollTop: 0
        }, duration)
        scroll_top_button.classList.add("o_hidden");
        return false;
    }
}
OecActionContainer.components.ScrollTopAction = ScrollTopAction;
OecActionContainer.template = xml `
<t t-name="web.ActionContainer">
  <div class="o_action_manager">
    <t t-if="info.Component" t-component="info.Component" className="'o_action'" t-props="info.componentProps" t-key="info.id">
    </t>
    <ScrollTopAction/>
  </div>
</t>`;