/** @odoo-module **/
import {
    session
} from "@web/session";
import {
    NavBar
} from "@web/webclient/navbar/navbar";
import {
    useService
} from "@web/core/utils/hooks";
import {
    registry
} from "@web/core/registry";
const systrayRegistry = registry.category("systray");

const {
    Component,
    onMounted,
    onPatched,
    useExternalListener,
    useState,
    useRef
} = owl;

export class OecNavBar extends NavBar {
    setup() {
        super.setup();

        this.menuAppsRef = useRef("menuApps");

        this.theme = session.theme;
        this.default_theme = this.getDefaultTheme(this.theme);

        this.main_menu_mode = this.default_theme["main_menu_mode"];
        this.main_submenu_position = this.default_theme["main_submenu_position"];
        this.sidebar_display_number_of_submenus = this.default_theme["sidebar_display_number_of_submenus"];
        this.sidebar_fixed = this.default_theme["sidebar_fixed"];
        this.sidebar_show_minimize_button = this.default_theme["sidebar_show_minimize_button"];
        this.sidebar_default_minimized = this.default_theme["sidebar_default_minimized"];
        this.sidebar_hover_maximize = this.default_theme["sidebar_hover_maximize"];

        this.sidebarMinimize = false;
        if (!this.sidebar_fixed || this.sidebar_default_minimized) {
            this.sidebarMinimize = true;
        }


        onMounted(() => {
            // 在渲染组件之后，
            this.body = document.body;

            // 移除主题设置 按钮
            const disable_customization = session.theme.disable_customization;
            if (disable_customization) {
                systrayRegistry.remove("ThemeConfigMenu");
            }

            if (this.env.isSmall && this.main_menu_mode == '1') {
                // this.env.bus.on("SIDENAV-MENU:TOGGLED", this, () => this._updateMenuAppsIcon());
                // this._updateMenuAppsIcon();
            }
        });
        onPatched(() => {
            this._updateMenuAppsIcon();
        });
    }

    _updateMenuAppsIcon() {
        // const menuAppsEl = this.menuSideNavBar.el;
    }

    getDefaultTheme(theme) {
        // 获取默认主题
        var self = this;
        var default_theme = {};

        // main
        if (self.hasKey("main_menu_mode", theme)) {
            default_theme["main_menu_mode"] = theme["main_menu_mode"];
        } else {
            default_theme["main_menu_mode"] = "1";
        }
        if (self.hasKey("main_submenu_position", theme)) {
            default_theme["main_submenu_position"] = theme["main_submenu_position"];
        } else {
            default_theme["main_submenu_position"] = "3";
        }

        // sidebar
        if (self.hasKey("sidebar_display_number_of_submenus", theme)) {
            default_theme["sidebar_display_number_of_submenus"] = theme["sidebar_display_number_of_submenus"];
        } else {
            default_theme["sidebar_display_number_of_submenus"] = true;
        }

        if (self.hasKey("sidebar_fixed", theme)) {
            default_theme["sidebar_fixed"] = theme["sidebar_fixed"];
        } else {
            default_theme["sidebar_fixed"] = true;
        }

        if (self.hasKey("sidebar_show_minimize_button", theme)) {
            default_theme["sidebar_show_minimize_button"] = theme["sidebar_show_minimize_button"];
        } else {
            default_theme["sidebar_show_minimize_button"] = false;
        }

        if (self.hasKey("sidebar_default_minimized", theme)) {
            default_theme["sidebar_default_minimized"] = theme["sidebar_default_minimized"];
        } else {
            default_theme["sidebar_default_minimized"] = false;
        }

        if (self.hasKey("sidebar_hover_maximize", theme)) {
            default_theme["sidebar_hover_maximize"] = theme["sidebar_hover_maximize"];
        } else {
            default_theme["sidebar_hover_maximize"] = false;
        }

        return default_theme;
    }

    hasKey(key, obj) {
        if (obj.hasOwnProperty(key)) {
            return true;
        } else {
            return false;
        }
    }
}

OecNavBar.template = "oec.OecNavBar";