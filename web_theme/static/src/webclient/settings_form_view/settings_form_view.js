/** @odoo-module **/

import {
    ScrollSettingsFormController
} from "./settings_form_controller";

import {
    settingsFormView
} from "@web/webclient/settings_form_view/settings_form_view";

settingsFormView.Controller = ScrollSettingsFormController;