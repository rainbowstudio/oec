/** @odoo-module **/

import {
    SettingsFormController
} from "@web/webclient/settings_form_view/settings_form_controller";

const {
    Component,
    onMounted,
    onRendered,
    onWillUpdateProps,
    onWillDestroy,
    onPatched,
    xml,
    hooks
} = owl;

export class ScrollSettingsFormController extends SettingsFormController {
    setup() {
        super.setup();
        onMounted(() => {
            this.onMounted();
        });
        onPatched(() => {
            this.onPatched();
        });
    }
    onRendered() {
        // 在渲染组件之前，会用来初始化一些数据等
        // console.log("onRendered");
    }
    onMounted() {
        // 在渲染组件之后，
        // console.log("onMounted");
        this.checkScrollElements();
    }
    onPatched() {
        // 在发生改变时执行， 此方法在元素根据新状态重新渲染之前进行调用。
        // console.log("onPatched");
    }

    //-----------------------------------------------
    // Handlers
    //-----------------------------------------------

    checkScrollElements() {
        let scrollEl = document.querySelector(".settings");
        // let scrollEl = document.querySelector(".app_settings_block");
        // console.log("scrollEl", scrollEl.clientHeight, scrollEl.scrollHeight);
        this.existsScrollEl = scrollEl !== null ? true : false;
        if (this.existsScrollEl) {
            this.showOrHideScrollButton(scrollEl);
        }
    }

    showOrHideScrollButton(scrollEl) {
        let self = this;
        const offset = 300;
        const scroll_top_button = document.querySelector(".o_scroll_top");
        scrollEl.addEventListener("scroll", function (ev) {
            const el = ev.target
            if (el.scrollTop > offset) {
                scroll_top_button.classList.remove("o_hidden");
            } else {
                scroll_top_button.classList.add("o_hidden");
            }
        });

        scroll_top_button.addEventListener("click", function (ev) {
            self.bindScrollSettingsFormEvent(ev, scrollEl)
        });
    }

    bindScrollSettingsFormEvent(ev, scrollEl) {
        let scroll_top_button = ev.target;
        if (scroll_top_button.tagName == "span") {
            scroll_top_button = scroll_top_button.parentNode;
        } else if (scroll_top_button.tagName == "svg") {
            scroll_top_button = scroll_top_button.parentNode.parentNode;
        } else if (scroll_top_button.tagName == "path") {
            scroll_top_button = scroll_top_button.parentNode.parentNode.parentNode;
        } else if (scroll_top_button.tagName == "rect") {
            scroll_top_button = scroll_top_button.parentNode.parentNode.parentNode;
        }
        const duration = 500; //时间
        ev.preventDefault();
        $(scrollEl).animate({
            scrollTop: 0
        }, duration)
        scroll_top_button.classList.add("o_hidden");
        return false;
    }
}