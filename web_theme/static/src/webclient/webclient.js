/** @odoo-module **/

import {
    browser
} from "@web/core/browser/browser";
import {
    useOwnDebugContext
} from "@web/core/debug/debug_context";
import {
    DebugMenu
} from "@web/core/debug/debug_menu";
import {
    localization
} from "@web/core/l10n/localization";
import {
    MainComponentsContainer
} from '@web/core/main_components_container';
import {
    registry
} from "@web/core/registry";
import {
    useBus,
    useService
} from "@web/core/utils/hooks";

import {
    ActionContainer
} from '@web/webclient/actions/action_container';

import {
    session
} from "@web/session";
const legacy_session = require('web.session'); // 需要访问allowed_company_ids，故使用 legacy 的代码，不能使用 @web/session
const {
    Component,
    onMounted,
    onPatched,
    useExternalListener,
    useState,
    useEffect,
} = owl;

import {
    SideNav
} from "./sidenav/sidenav";

import {
    OecNavBar
} from "./navbar/navbar";
import {
    OecActionContainer
} from "./actions/action_container";

export class WebClientOec extends Component {
    setup() {
        // this.sm = useService("mobile_sidenav_menu");
        // useService("oec_legacy_service_provider");

        this.menuService = useService("menu");
        this.actionService = useService("action");
        this.title = useService("title");
        this.router = useService("router");
        this.user = useService("user");

        // 主题
        this.theme = legacy_session.theme;
        this.default_theme = this.getDefaultTheme(this.theme);
        this.main_menu_mode = this.default_theme["main_menu_mode"]; // 全局主菜单模式
        this.sidebar_fixed = this.default_theme["sidebar_fixed"]; // 侧边栏固定
        this.sidebar_default_minimized = this.default_theme["sidebar_default_minimized"]; // 侧边栏默认折叠
        this.sidebar_hover_maximize = this.default_theme["sidebar_hover_maximize"]; // 侧边栏悬停展开

        this.sidebarMinimize = false;
        if (!this.sidebar_fixed || this.sidebar_default_minimized) {
            this.sidebarMinimize = true;
        }


        useService("legacy_service_provider");
        // useService("oec_legacy_service_provider");

        useOwnDebugContext({
            categories: ["default"]
        });
        if (this.env.debug) {
            registry.category("systray").add(
                "web.debug_mode_menu", {
                    Component: DebugMenu,
                }, {
                    sequence: 100
                }
            );
        }
        this.localization = localization;
        this.state = useState({
            fullscreen: false,
        });

        let system_name = legacy_session.system_name;
        let current_cid = legacy_session.user_context.allowed_company_ids[0]; //当前公司id
        if (legacy_session.display_company_name) {
            let allowed_companies = legacy_session.user_companies.allowed_companies;
            let current_company_name = getCurrentCompanyName(); //当前公司名称
            function getCurrentCompanyName() {
                for (var key in allowed_companies) {
                    let company = allowed_companies[key];
                    if (company.id === current_cid) {
                        return company.name;
                    }
                }
            }
            system_name = _.str.sprintf("%s - %s", current_company_name, system_name);
        }

        this.title.setParts({
            zopenerp: system_name
        });

        useBus(this.env.bus, "ROUTE_CHANGE", this.loadRouterState);
        useBus(this.env.bus, "ACTION_MANAGER:UI-UPDATED", ({
            detail: mode
        }) => {
            if (mode !== "new") {
                this.state.fullscreen = mode === "fullscreen";
            }
        });
        onMounted(() => {
            // 定义挂载组件时应该执行的代码的钩子
            this.loadRouterState();
            // 聊天窗口和对话服务侦听“web_client_ready”事件以初始化自身：
            this.env.bus.trigger("WEB_CLIENT_READY");

            // 设置body data
            this.el = document.body;
            // 使用 dataset 操作data要比使用getAttribute稍微慢些.
            this.el.setAttribute("data-menu-mode", this.main_menu_mode);
            this.el.setAttribute("data-sidebar-fixed", this.sidebar_fixed);
            this.el.setAttribute("data-sidebar-default-minimized", this.sidebar_default_minimized);
            this.el.setAttribute("data-sidebar-hover-maximize", this.sidebar_hover_maximize);

            if (this.sidebarMinimize) {
                this.el.setAttribute("data-sidebar-minimize", "on");
            } else {
                this.el.setAttribute("data-sidebar-minimize", "off");
            }
        });

        useExternalListener(window, "click", this.onGlobalClick, {
            capture: true
        });
    }

    async loadRouterState() {
        let stateLoaded = await this.actionService.loadState();
        let menuId = Number(this.router.current.hash.menu_id || 0);
        if (!stateLoaded && menuId) {
            // Determines the current actionId based on the current menu
            const menu = this.menuService.getAll().find((m) => menuId === m.id);
            const actionId = menu && menu.actionID;
            if (actionId) {
                await this.actionService.doAction(actionId, {
                    clearBreadcrumbs: true
                });
                stateLoaded = true;
            }
        }

        if (stateLoaded && !menuId) {
            // Determines the current menu based on the current action
            const currentController = this.actionService.currentController;
            const actionId = currentController && currentController.action.id;
            const menu = this.menuService.getAll().find((m) => m.actionID === actionId);
            menuId = menu && menu.appID;
        }

        if (menuId) {
            // Sets the menu according to the current action
            this.menuService.setCurrentMenu(menuId);
        }

        if (!stateLoaded) {
            // If no action => falls back to the default app
            await this._loadDefaultApp();
        }
    }

    _loadDefaultApp() {
        // Selects the first root menu if any
        const root = this.menuService.getMenu("root");
        const firstApp = root.children[0];
        if (firstApp) {
            return this.menuService.selectMenu(firstApp);
        }
    }

    /**
     * @param {MouseEvent} ev
     */
    onGlobalClick(ev) {
        // When a ctrl-click occurs inside an <a href/> element
        // we let the browser do the default behavior and
        // we do not want any other listener to execute.
        if (
            ev.ctrlKey &&
            ((ev.target instanceof HTMLAnchorElement && ev.target.href) ||
                (ev.target instanceof HTMLElement && ev.target.closest("a[href]:not([href=''])")))
        ) {
            ev.stopImmediatePropagation();
            return;
        }
    }

    _updateClassList() {

    }

    getDefaultTheme(theme) {
        // 获取默认主题
        var self = this;
        var default_theme = {};

        // main
        if (self.hasKey("main_menu_mode", theme)) {
            default_theme["main_menu_mode"] = theme["main_menu_mode"];
        } else {
            default_theme["main_menu_mode"] = "1";
        }
        if (self.hasKey("main_submenu_position", theme)) {
            default_theme["main_submenu_position"] = theme["main_submenu_position"];
        } else {
            default_theme["main_submenu_position"] = "3";
        }

        // sidebar
        if (self.hasKey("sidebar_display_number_of_submenus", theme)) {
            default_theme["sidebar_display_number_of_submenus"] = theme["sidebar_display_number_of_submenus"];
        } else {
            default_theme["sidebar_display_number_of_submenus"] = true;
        }

        if (self.hasKey("sidebar_fixed", theme)) {
            default_theme["sidebar_fixed"] = theme["sidebar_fixed"];
        } else {
            default_theme["sidebar_fixed"] = true;
        }

        if (self.hasKey("sidebar_show_minimize_button", theme)) {
            default_theme["sidebar_show_minimize_button"] = theme["sidebar_show_minimize_button"];
        } else {
            default_theme["sidebar_show_minimize_button"] = false;
        }

        if (self.hasKey("sidebar_default_minimized", theme)) {
            default_theme["sidebar_default_minimized"] = theme["sidebar_default_minimized"];
        } else {
            default_theme["sidebar_default_minimized"] = false;
        }

        if (self.hasKey("sidebar_hover_maximize", theme)) {
            default_theme["sidebar_hover_maximize"] = theme["sidebar_hover_maximize"];
        } else {
            default_theme["sidebar_hover_maximize"] = false;
        }

        return default_theme;
    }

    hasKey(key, obj) {
        if (obj.hasOwnProperty(key)) {
            return true;
        } else {
            return false;
        }
    }
}


WebClientOec.components = {
    // ActionContainer,
    ActionContainer: OecActionContainer,
    // ContentScrollTop,
    NavBar: OecNavBar,
    MainComponentsContainer,
    SideNav,
};
WebClientOec.template = "oec.WebClient";