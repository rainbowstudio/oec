/** @odoo-module **/

const {
    Component,
} = owl;

export class ScrollTopAction extends Component {
    setup() {
        super.setup();
    }
}

ScrollTopAction.template = "web.ScrollTopAction";