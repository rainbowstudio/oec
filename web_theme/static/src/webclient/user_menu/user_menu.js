/** @odoo-module **/

import {
    Dropdown
} from "@web/core/dropdown/dropdown";
import {
    DropdownItem
} from "@web/core/dropdown/dropdown_item";
import {
    CheckBox
} from "@web/core/checkbox/checkbox";
import {
    browser
} from "@web/core/browser/browser";
import {
    registry
} from "@web/core/registry";
import {
    useService
} from "@web/core/utils/hooks";

import {
    UserMenu
} from "@web/webclient/user_menu/user_menu";

const {
    Component
} = owl;

const userMenuRegistry = registry.category("user_menuitems");

export class OecUserMenu extends UserMenu {
    setup() {
        super.setup();
        // this.isDebug = Boolean(odoo.debug);
        // this.isAssets = odoo.debug.includes("assets");
    }

    // getElements() {
    //     // const sortedItems = super.getElements();
    //     // console.log("sortedItems", sortedItems);
    //     // return sortedItems.filter((item) => item.active === true);
    // }
}