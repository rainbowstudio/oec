/** @odoo-module **/

import {
    patch
} from "@web/core/utils/patch";
import spreadsheet from "@spreadsheet/o_spreadsheet/o_spreadsheet_extended";
import {
    useService
} from "@web/core/utils/hooks";
const {
    onMounted,
    onPatched,
    onWillDestroy,
    useRef,
} = owl;


patch(spreadsheet.components.Grid.components.VerticalScrollBar.prototype, "spreadsheet.Grid.VerticalScrollBar", {
    setup() {
        this._super();

        const {
            offsetScrollbarY
        } = this.env.model.getters.getActiveSheetScrollInfo();
        this.offsetScrollbarY = offsetScrollbarY;

        this.max_offset = 300;
        this.router = useService("router");
        this.currentDashboardId = Number(this.router.current.hash.dashboard_id || 0);

        if (this.currentDashboardId !== 0) {
            onMounted(() => {
                this.onMounted();
            });
            onPatched(() => {
                this.onPatched();
            });
            onWillDestroy(() => {
                this.onWillDestroy();
            });
        }
    },

    onMounted() {
        // 在渲染组件之前，会用来初始化一些数据等
        let self = this;
        this.scrollingEl = document.querySelector(".o-scrollbar.vertical");
        if (this.scrollingEl) {
            this.scroll_top_button = document.querySelector(".o_scroll_top");
            if (this.scroll_top_button) {
                this.scroll_top_button.addEventListener("click", this.onScrollTop.bind(this));
            }
            this.showOrHideScrollTopButton();
        }
    },
    onPatched() {
        // 在发生改变时执行， 此方法在元素根据新状态重新渲染之前进行调用。
        // console.log("onPatched", this.scrollingEl.scrollTop)

        this.showOrHideScrollTopButton();
        // console.log("onPatched scroll", this.offsetScrollbarY);
    },
    onWillDestroy() {
        // 定义在卸载组件之前应执行的代码
        // this.offsetScrollbarY = 0;

        this.showOrHideScrollTopButton();
        // console.log("onWillDestroy", this.offsetScrollbarY);
    },

    //-----------------------------------------------
    // Handlers
    //-----------------------------------------------
    onScroll(offset) {
        this._super(...arguments);
        const {
            offsetScrollbarY
        } = this.env.model.getters.getActiveSheetScrollInfo();
        this.offsetScrollbarY = offsetScrollbarY;
        // console.log("onScroll", this.offsetScrollbarY)

    },
    onScrollTop(ev) {
        let self = this;
        ev.preventDefault();
        const duration = 500; //时间延迟
        $(self.scrollingEl).animate({
            scrollTop: 0
        }, duration)
        return false;
    },
    showOrHideScrollTopButton() {
        if (this.offsetScrollbarY > this.max_offset) {
            this.scroll_top_button.classList.remove("o_hidden");
        } else {
            this.scroll_top_button.classList.add("o_hidden");
        }
    },
});