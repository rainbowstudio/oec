/** @odoo-module **/

import {
    browser
} from "@web/core/browser/browser";
import {
    registry
} from "@web/core/registry";

// export const legacyServiceProvider = {
//     dependencies: ["mobile_sidenav_menu"],
//     start({
//         services
//     }) {
//         browser.addEventListener("show-mobile-sidenav-menu", () => {
//             services.sidenav_menu.toggle(true);
//         });
//     },
// };

// registry.category("services").add("oec_legacy_service_provider", legacyServiceProvider);