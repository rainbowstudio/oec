/** @odoo-module **/

import {
    startWebClient
} from "@web/start";
// import {
//     WebClient
// } from "@web/webclient";
// import {
//     WebClient
// } from "./webclient/webclient";
import {
    WebClientOec
} from "./webclient/webclient";

/**
  此文件启动  china_rtp webclient。在清单中，它将替换社区版 main.js 加载不同的webclient类（ WebClientOec 而不是 webclient）
 */

// startWebClient(WebClient);
startWebClient(WebClientOec);