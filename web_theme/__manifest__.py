# -*- coding: utf-8 -*-

{
    "name": "Web Theme",
    "author": "RStudio",
    "website": "https://eis-solution.coding.net/public/odoo/oec/git",
    "sequence": 500,
    "installable": True,
    "application": True,
    "auto_install": False,
    "category": "Themes/Backend",
    "version": "16.0.0.1",
    "summary": "Backend theme with sidebar",
    "description": """
Web Client.
===========================

This module modifies the web plug-in to provide the following functions:
---------------------------
1) Responsiveness
2) Sidebars
3) Supports more than four levels of menus
4) Scroll to top button
5) Support custom browser window title name

""",
    "depends": ["web", "mail", "calendar", "portal", "spreadsheet"],
    "external_dependencies": {
        "python": [],
    },
    "data": [
        "security/ir.model.access.csv",
        "data/res_theme_data.xml",
        "data/res_company_data.xml",
        "views/webclient_templates.xml",
        "views/res_company_views.xml",
        "views/res_users_views.xml",
        "views/calendar_views.xml",
    ],
    "assets": {
        # -----------------------
        # MAIN BUNDLES
        # 主约束
        # -----------------------
        # 这些是在常规XML模板中通过“t-call-assets”调用的捆绑包。
        # 命名捆绑包的约定如下：
        # 1）定义捆绑包的第一个模块的名称
        # 2）前缀“assets_”
        # 3）与捆绑包内容相关的任意名称。
        #
        # 例如：
        # > web.assets_common = 前端和后端客户端共用的资产。
        # > web_editor.assets_wysiwyg = 在“web_编辑器”模块中定义的组件所需的资产。
        # "web.assets_qweb": [
        #     "web_theme/static/src/**/*.xml",
        #     "web_theme/static/src/**/**/*.variables.scss",
        # ],
        "web.assets_common": [
            "web_theme/static/fonts/fonts.scss",
            "web_theme/static/fonts/password.scss",
            "web_theme/static/libs/bootstrap-icons/bootstrap-icons.css",
        ],
        "web.assets_backend": [
            "web_theme/static/src/legacy/**/*.js",
            "web_theme/static/src/webclient/**/*",
            "web_theme/static/src/components/**/*",
            # CSS
            (
                "replace",
                "web/static/src/webclient/webclient_layout.scss",
                "web_theme/static/src/webclient/webclient_layout.scss",
            ),
            # js
            (
                "replace",
                "web/static/src/webclient/webclient.js",
                "web_theme/static/src/webclient/webclient.js",
            ),
            (
                "replace",
                "web/static/src/webclient/user_menu/user_menu_items.js",
                "web_theme/static/src/webclient/user_menu/user_menu_items.js",
            ),

        ],
        "web.assets_backend_legacy_lazy": [],
        "web.assets_frontend_minimal": [],
        "web.assets_frontend": [

        ],
        "web.assets_frontend_lazy": [],
        "web.assets_backend_prod_only": [
            ("replace", "web/static/src/main.js", "web_theme/static/src/main.js"),
        ],
        "web.pdf_js_lib": [],
        "web.report_assets_common": [],
        "web.report_assets_pdf": [],
        # -----------------------
        # SUB BUNDLES
        # 子约束
        # 这些捆绑包可以由主捆绑包使用，但不应直接从XML模板调用。
        # 它们的命名约定与主捆绑包的命名约定类似，只是添加了前缀下划线以反映“私有”方面。
        #
        # 示例：
        # > web._assets_helpers = 定义大多数主捆绑包中所需的资产
        # -----------------------
        "web._assets_primary_variables": [
            (
                "prepend",
                "web_theme/static/src/legacy/scss/primary_variables.scss",
            ),
            "web_theme/static/src/**/**/*.variables.scss",
        ],
        "web._assets_secondary_variables": [
            "web_theme/static/src/legacy/scss/secondary_variables.scss",
        ],
        "web._assets_helpers": [],
        "web._assets_bootstrap": [],
        "web._assets_backend_helpers": [],
        "web._assets_frontend_helpers": [],
        # 在web架构转换期间使用
        "web.frontend_legacy": [],

        # 锁屏样式
        "web.assets_lock": [
            "web/static/src/libs/fontawesome/css/font-awesome.css",
            "web/static/lib/bootstrap/dist/css/bootstrap.css",
            "web/static/lib/jquery/jquery.js",
            "web/static/lib/bootstrap/js/dist/dom/data.js",
            "web/static/lib/bootstrap/js/dist/dom/event-handler.js",
            "web/static/lib/bootstrap/js/dist/dom/manipulator.js",
            "web/static/lib/bootstrap/js/dist/dom/selector-engine.js",
            "web/static/lib/bootstrap/js/dist/base-component.js",
            "web/static/lib/bootstrap/js/dist/modal.js",

            "web_theme/static/libs/bootstrap-icons/bootstrap-icons.css",
            "web_theme/static/fonts/password.scss",
            "web_theme/static/src/lockclient/*",
        ],
    },
    "bootstrap": True,  # 加载登录屏幕的翻译，
    "post_init_hook": "post_init_hook",  # 安装后执行的方法
    "license": "Other proprietary",
}
