# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class ResTheme(models.Model):
    _name = "res.theme"
    _description = "Theme"

    name = fields.Char(
        string="Name",
        copy=False,
        compute="_compute_name",
        store=True,
        index=True,
    )  # 企业应用名称

    company_id = fields.Many2one(
        string="Company", comodel_name="res.company", ondelete="cascade", readonly=True
    )
    user_id = fields.Many2one(
        string="User", comodel_name="res.users", ondelete="cascade", readonly=True
    )

    type = fields.Selection(
        string="Type",
        selection=[("user", "User"), ("company", "Company")],
        # compute="_compute_company_type",
        # inverse="_write_company_type",
    )

    disable_theme_customizer = fields.Boolean(
        string="Disable theme customizer", default=False
    )

    # ------------------------------------------------------------
    # main
    # ------------------------------------------------------------
    main_menu_mode = fields.Selection(
        string="Menu mode",
        selection=[
            ("1", "Sidebar"),
        ],
        default="1",
        readonly=False,
    )  # ("2", "Favorites"), ("3", "Drawer")
    main_submenu_position = fields.Selection(
        string="Submenu location",
        selection=[
            ("1", "Navbar menu"),
            ("2", "Sidebar menu"),
            ("3", "Navbar menu and Sidebar menu"),
        ],
        default="3",
        readonly=False,
    )

    # ------------------------------------------------------------
    # SideNavbar
    # ------------------------------------------------------------
    sidebar_display_number_of_submenus = fields.Boolean(
        string="Display Number Of Submenus", default=True
    )
    sidebar_fixed = fields.Boolean(string="Fixed SideNavbar", default=True)
    sidebar_show_minimize_button = fields.Boolean(
        string="Show minimize button", default=True
    )
    sidebar_default_minimized = fields.Boolean(
        string="Default minimize", default=False
    )
    sidebar_hover_maximize = fields.Boolean(
        "Hover maximize", default=True
    )

    @api.depends("company_id", "user_id", "type")
    def _compute_name(self):
        for theme in self:
            labels = dict(self.fields_get(allfields=["type"])["type"]["selection"])[
                theme.type
            ]
            if theme.company_id:
                theme.name = "%s:%s" % (labels, theme.company_id.name)
            else:
                theme.name = "%s:%s" % (labels, theme.user_id.name)

    def _get_or_create_theme(self, id, type):
        """
        通过id和type获取或者创建theme
        """
        domain = []
        vals = {}
        if type == "company":
            domain = [("company_id", "=", id), ("type", "=", "company")]
            vals = {"company_id": id, "type": "company"}
        elif type == "user":
            domain = [("user_id", "=", id), ("type", "=", "user")]
            vals = {"user_id": id, "type": "user"}
        theme = self.search(domain, limit=1)

        if not theme:
            theme = self.create(vals)

        return theme
