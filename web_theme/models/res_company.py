# -*- coding: utf-8 -*-

import base64
from email.policy import default
import io
import os

from odoo import models, fields, api, tools, _
from odoo.modules.module import get_module_resource, get_resource_path
from random import randrange
from PIL import Image


class ResCompany(models.Model):
    _inherit = "res.company"

    def _get_theme_favicon(self, original=False):
        img_path = get_resource_path("web_theme", "static/img/favicon.ico")
        with tools.file_open(img_path, "rb") as f:
            if original:
                return base64.b64encode(f.read())
            # Modify the source image to add a colored bar on the bottom
            # This could seem overkill to modify the pixels 1 by 1, but
            # Pillow doesn't provide an easy way to do it, and this
            # is acceptable for a 16x16 image.
            color = (
                randrange(32, 224, 24),
                randrange(32, 224, 24),
                randrange(32, 224, 24),
            )
            original = Image.open(f)
            new_image = Image.new("RGBA", original.size)
            height = original.size[1]
            width = original.size[0]
            bar_size = 1
            for y in range(height):
                for x in range(width):
                    pixel = original.getpixel((x, y))
                    if height - bar_size <= y + 1 <= height:
                        new_image.putpixel((x, y), (color[0], color[1], color[2], 255))
                    else:
                        new_image.putpixel(
                            (x, y), (pixel[0], pixel[1], pixel[2], pixel[3])
                        )
            stream = io.BytesIO()
            new_image.save(stream, format="ICO")
            return base64.b64encode(stream.getvalue())

    def _default_theme(self):
        return self.env["res.theme"].sudo()._get_or_create_theme(self.id, "company")

    def _get_square_logo(self):
        img_path = get_module_resource("web_theme", "static", "img", "square_logo.png")
        return base64.b64encode(open(img_path, "rb").read())

    square_logo = fields.Binary(
        default=_get_square_logo,
        # related="partner_id.image_1920",
        string="Company Square Logo",
        readonly=False,
    )
    square_logo_web = fields.Binary(
        compute="_compute_square_logo_web", store=True, attachment=False
    )

    @api.depends("square_logo")
    def _compute_square_logo_web(self):
        for company in self:
            img = company.square_logo
            company.square_logo_web = img and base64.b64encode(
                tools.image_process(base64.b64decode(img), size=(180, 0))
            )

    theme_id = fields.Many2one(
        "res.theme",
        string="Theme",
        store=True,
        domain="[('company_id', '=', id)]",
    )
    disable_theme_customizer = fields.Boolean(
        string="Disable theme customizer",
        related="theme_id.disable_theme_customizer",
        readonly=False,
    )

    # ------------------------------------------------------------
    # main
    # ------------------------------------------------------------
    main_menu_mode = fields.Selection(related="theme_id.main_menu_mode", readonly=False)
    main_submenu_position = fields.Selection(
        related="theme_id.main_submenu_position",
        readonly=False,
    )

    # ------------------------------------------------------------
    # SideNavbar
    # ------------------------------------------------------------
    sidebar_display_number_of_submenus = fields.Boolean(
        related="theme_id.sidebar_display_number_of_submenus", readonly=False
    )
    sidebar_fixed = fields.Boolean(related="theme_id.sidebar_fixed", readonly=False)
    sidebar_show_minimize_button = fields.Boolean(
        related="theme_id.sidebar_show_minimize_button", readonly=False
    )
    sidebar_default_minimized = fields.Boolean(
        related="theme_id.sidebar_default_minimized", readonly=False
    )
    sidebar_hover_maximize = fields.Boolean(
        related="theme_id.sidebar_hover_maximize", readonly=False
    )

    # 用户菜单
    menuitem_id = fields.Many2one(
        "res.user.menuitems",
        string="User menu items",
        store=True,
        domain="[('company_id', '=', id)]",
    )
    enable_odoo_account = fields.Boolean(related="menuitem_id.enable_odoo_account", readonly=False)

    enable_lock_screen = fields.Boolean(related="menuitem_id.enable_lock_screen", readonly=False)

    lock_screen_state_storage_mode = fields.Selection(string="Lock screen state storage mode",selection=[
            ("1", "Use browser's local storage"),
            ("2", "Use database"),
        ],
        default="1",)

    enable_developer_tool = fields.Boolean(related="menuitem_id.enable_developer_tool", readonly=False)
    enable_documentation = fields.Boolean(related="menuitem_id.enable_documentation", readonly=False)
    documentation_url = fields.Char(related="menuitem_id.documentation_url", readonly=False)
    enable_support = fields.Boolean(related="menuitem_id.enable_support", readonly=False)
    support_url = fields.Char(related="menuitem_id.support_url", readonly=False)


    @api.model_create_multi
    def create(self, vals_list):
        """
        创建新公司时，创建主题
        """
        companies = super(ResCompany, self).create(vals_list)
        for new_company in companies:
            new_company.theme_id = self.env["res.theme"].sudo()._get_or_create_theme(new_company.id, "company")

            new_company.menuitem_id = self.env["res.user.menuitems"].sudo()._get_or_create_menuitems(new_company.id)

        return companies
