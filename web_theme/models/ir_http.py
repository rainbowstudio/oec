# -*- coding: utf-8 -*-

import json
from odoo import api, models
from odoo.http import request
from odoo.tools import ustr


class Http(models.AbstractModel):
    _inherit = "ir.http"

    def webclient_rendering_context(self):
        """
        覆盖社区版,以防止不必要的加载请求
        """
        return {
            "session_info": self.session_info(),
        }

    def session_info(self):
        """
        设置用户的主题信息 和锁屏信息
        """
        session_info = super(Http, self).session_info()

        # 语言
        langs = self.env["res.lang"].search_read([], ["name", "code", "flag_image_url"])
        for lang in langs:
            if lang["code"] == request.env.lang:
                session_info["current_lang"] = lang
                break
        session_info["langs"] = langs

        current_user = self.env.user
        current_user_company = current_user.company_id

        # 主题
        disable_theme_customizer = (
            current_user_company.theme_id.disable_theme_customizer
        )
        theme = {}

        theme_id = current_user.theme_id
        if disable_theme_customizer:
            # 如果关闭用户定制主题功能，则使用公司绑定的主题
            theme_id = current_user_company.theme_id
        theme = {
            "disable_customization": disable_theme_customizer,
            "main_menu_mode": theme_id.main_menu_mode,
            "main_submenu_position": theme_id.main_submenu_position,
            "sidebar_display_number_of_submenus": theme_id.sidebar_display_number_of_submenus,
            "sidebar_fixed": theme_id.sidebar_fixed,
            "sidebar_show_minimize_button": theme_id.sidebar_show_minimize_button,
            "sidebar_default_minimized": theme_id.sidebar_default_minimized,
            "sidebar_hover_maximize": theme_id.sidebar_hover_maximize,
        }
        session_info.update({"theme": json.loads(json.dumps(theme))})

        # 锁屏方式
        session_info.update(
            {
                "lock_screen_state_storage_mode": int(current_user_company.lock_screen_state_storage_mode),
                # "lock_screen_state":False,
            }
        )
        # print(session_info)
        return session_info

    def get_frontend_session_info(self):
        session_info = super(Http, self).get_frontend_session_info()

        # 锁屏方式
        session_info.update(
            {
                "lock_screen_state_storage_mode": int(self.env.user.company_id.lock_screen_state_storage_mode),
                # "lock_screen_state":False,
            }
        )

        return session_info



    # @api.model
    # def set_lock_session_info(self, uid, lock_info):
    #     """
    #     设置锁屏的session信息
    #     """
    #     session_info = self.env['ir.http'].session_info()
    #     frontend_session_info = self.env['ir.http'].get_frontend_session_info()
    #     storage_mode = session_info["lock_screen_state_storage_mode"]
    #     lock = {
    #         "uid": uid,
    #         "href": lock_info["href"],
    #         "host": lock_info["host"],
    #         "pathname": lock_info["pathname"],
    #         "search": lock_info["search"],
    #         "hash": lock_info["hash"],
    #     }
    #     if "lock" in lock_info:
    #         lock["lock"] = lock_info["lock"]
    #     if storage_mode ==1:
    #         session_info.update({"lock_screen_state": True})
    #         frontend_session_info.update({"lock_screen_state": True})
    #     if storage_mode ==2:
    #         user = self.env["res.users"].sudo().search([('id', '=', uid)], limit=1)
    #         user.write({"lock_screen": True})

    #     request.session["lock_screen_info"] = json.loads(json.dumps(lock))
    #     request.session.modified = True  # 标记session已修改
    #     return request.session["lock_screen_info"]
