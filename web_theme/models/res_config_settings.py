# -*- coding: utf-8 -*-

import json
import logging
import base64
import io

from odoo.tools.misc import file_open
from odoo import api, fields, models, tools, SUPERUSER_ID, _
from odoo.modules.module import get_resource_path
from random import randrange
from PIL import Image


class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"

    