# China Erp Widget

## `专业版升级提示弹框`
功能：开发者可以在 视图 `res_config_settings_view_form` 中添加标签 ```<widget name="oec_pro_upgrade_boolean" />``` 来引流到自己的项目中。
```xml
<div class="col-12 col-lg-6 o_setting_box" id="rainbow_community_theme_install">
    <div class="o_setting_left_pane">
        <field name="module_rainbow_community_theme" widget="oec_pro_upgrade_boolean" url="https://eis-solution.coding.net/public/odoo/oec/git" image="/web/static/img/enterprise_upgrade.jpg"/>
    </div>
    <div class="o_setting_right_pane" id="rainbow_community_theme_settings">
        <label for="module_rainbow_community_theme"/>
        <div class="text-muted">
            This module modifies the web addon to provide responsiveness.
        </div>
        <div class="content-group" attrs="{'invisible': [('module_rainbow_community_theme', '=', False)]}" id="rainbow_community_theme_warning">
            <div class="mt4 text-warning">
                <strong>Save</strong> this page and come back here to set up other feature.
            </div>
        </div>
    </div>
</div>
```

> 说明：

1. `<field>`标签的 `widget` 属性值为 `oec_pro_upgrade_boolean`，表示使用`oec_pro_upgrade_boolean`组件
2. `<field>`标签的 `url` 属性值缺省值为 `https://eis-solution.coding.net/public/odoo/oec/git`，
2. `<field>`标签的 `image` 属性值缺省值为 `/web/static/img/enterprise_upgrade.jpg`，

##  `一对多配置`
功能：显示配置帮助说明
```xml
<field name="contacts_app_config_ids" class="w-100 pl-3" widget="one2many_config" help="description" t-translation="off">
    <tree create="0" edit="true" delete="0" editable="bottom" sample="1">
        <field name="name" readonly="1"/>
        <field name="key" readonly="1"/>
        <field name="value" need_format="true" force_save="1" required="1"/>
        <field name="description" invisible="1"/>
    </tree>
</field>
```

> 说明：

1. `<field>`标签的`widget`属性值为`one2many_config`，表示使用`one2many_config`组件
2. `<field>`标签的`help`属性值为`description`，表示使用`description`字段的值作为`one2many_config`组件的`help`属性值
3. `<field>`标签的`t-translation`属性值务必为`off`，否则会导致`one2many_config`组件的`help`属性值无法正常显示
4. `<tree>` 建议属性如：`create="0" edit="true" delete="0"`, `<tree>`中的需要对应`help`的字段，且必须设置`invisible="1"`



## `文本字符串密码化`
功能：使用字体来处理密码，避免浏览器记住密码文本
```xml
<field name="contacts_secret" widget="DisplayPasswordChar" require_encryption="True" />
```

> 说明：

1. `<field>`标签的`widget`属性值为`DisplayPasswordChar`，表示使用`DisplayPasswordChar`组件
2. `<field>`标签的`require_encryption`缺省值为`true`，表示使用`DisplayPasswordChar`组件时，必须加密字符串