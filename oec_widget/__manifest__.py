# -*- coding: utf-8 -*-

{
    "name": "Open ERP China Widget",
    "author": "RStudio",
    "category": "OEC Suites/Widget",
    "summary": "Open ERP China Widget",
    "website": "https://eis-solution.coding.net/public/odoo/oec/git",
    "version": "16.0.0.1",
    "description": """ 

""",
    "depends": [
        "web",
    ],
    "data": [],
    "assets": {
        "web.assets_common": [
            "oec_widget/static/fonts/fonts.scss",
        ],
        "web.assets_backend": [
            "oec_widget/static/src/views/**/*",
            "oec_widget/static/src/webclient/**/*",
        ],
    },
    "sequence": 500,
    "installable": True,
    "auto_install": True,
    "application": False,
    "license": "Other proprietary",
}
